const {Schema , model} = require('mongoose')
const userScheme     = new Schema({
    password :{
        type : String , 
        required : true
    } , 
    username :{
        type : String , 
        required : true
    } , 
    salt     : {
        type : String ,
        required : true
    } ,
    role     : {
        type : String
    }

},{timestamps:true})

userScheme.methods.generateJwt = function(){
    
}

const User = model("users" , userScheme)

module.exports = User
