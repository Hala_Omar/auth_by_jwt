const {Schema , model } = require('mongoose')

const access_token_schema = new Schema ( {
    token : {
        type : String , 
        required : true
    } ,
    createdAt : {
        type : Date , 
        default : Date.now() ,
        index : {
            expires : 300000
        }
    }

} , { timestamps : true })

const Token = model('access_tokens' , access_token_schema)

module.exports = Token 