const passportJWT = require('passport-jwt')
const JwtStrategy    = passportJWT.Strategy
const ExtractJWT  = passportJWT.ExtractJwt
const fs = require('fs');
const path = require('path');
const User  = require('../database/User')
const pathToKey = path.join(__dirname , '..' , '/config/rsa-pub-key.pem')
const  publicKey  = fs.readFileSync(`${__dirname}/rsa-pub-key.pem`,"utf-8")

// console.log('publickey :>> ', publicKey);
    const options = { 
        jwtFromRequest : ExtractJWT.fromAuthHeaderAsBearerToken(),
        secretOrKey    : publicKey , //because here we will verify not issuance(Private key)
        algorithms     : ['RS256'] 
    }

const jwtStrategy = new JwtStrategy( options , ( payload , done) =>{

    console.log('payload :>> ', payload);
    User.find({
        _id : payload.sub
        }).then( (user) => {
            if(user){
                //attach the passport object to req
           return done(null , user)
            }else{
            return done(null , false)
            }
        })
        .catch( (error) => {done( error , false)})
})

module.exports = ( passport ) =>{
    passport.use(jwtStrategy)
}