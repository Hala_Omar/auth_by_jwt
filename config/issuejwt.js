const base64url = require('base64url')//the standard format for jwt parts
const crypto = require('crypto')
const fs = require('fs')
const signatureFunction = crypto.createSign('RSA-SHA265')//writable stream
const privateKey = fs.readFileSync(`${__dirname}/rsa-priv-key.pem`)

const header = {

}
const payload = {

}


const headerBase64url = base64url(JSON.stringify(header))
const payloadBase64url = base64url(JSON.stringify(payload))

signatureFunction.write(headerBase64url +'.'+payloadBase64url)
signatureFunction.end()
// hash the `headerBase64url +'.'+payloadBase64url` and sign it with private 
// key and return the signature as base64 encoded signature
const signatureBase64 =  signatureFunction.sign(privateKey , 'base64')//return buffer by default
const signatureBase64url = base64url.fromBase64(signatureBase64)

module.exports = ( user ) =>{

    let payload ={
        sub : user._id ,
        iat : Date.now()
    }
}

