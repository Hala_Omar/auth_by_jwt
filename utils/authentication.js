const crypto = require('crypto')
const jsonwebtoken = require('jsonwebtoken')
const fs = require('fs')
const user_dao   = require('../users/user_dao')

exports.checkCredentials = async ( userCredentials ) => { 

    let user =  await user_dao.getUser({username : userCredentials.username})
    
    if(user){

        let password = userCredentials.password
        let salt     = user.salt

        let hashedPassword = crypto.pbkdf2Sync(password , salt , 1000 , 64 , 'sha1').toString('hex')
        console.log('hashedPassword :>> ', hashedPassword);

        hashedPassword == user.password ? 
        user = user                 :
        user = false
        
    }else{
        user = false
    }

    return user
}

exports.issueJwt = ( user ) =>{

    let payload = {
        sub : user._id , 
        iat : Date.now()
    }

    let privateKey = fs.readFileSync( __dirname +'\\rsa-priv-key.pem' , 'utf-8')
    let jwt = jsonwebtoken.sign(payload , privateKey , {

        expiresIn : "2 days" , 
        algorithm : "RS256"
    } ) 
    

    return {
        jwt ,
        expiredIn : 24*60*60*1000
    }

}

exports.getHashAndSalt = ( password ) => {

        const salt = crypto.randomBytes(32).toString('hex')
        const hashedPassword = crypto.pbkdf2Sync(password,salt,1000,64,"sha1").toString('hex')
    
        return {
            salt , 
            hashedPassword
        }
    
    }


