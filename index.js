const express = require('express')
require('dotenv').config({ path: `${__dirname}/.env` })
const passport = require('passport');
const app = express()
require('./database/connect')
require('./config/passport')(passport)

app.use(express.json())
app.use(passport.initialize()) // passport.initialize() will initialize the passport object on every request

app.use('/api/v1/user' , require('./users/web_app/users_router'))


app.listen(process.env.PORT , ()=>{

    console.log(`The server run on port 3000${process.env.PORT}`);
})