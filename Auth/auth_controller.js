
const passport = require('passport')

exports.authenticate = ( req, res , next ) =>{
    console.log('req.headers.authorization :>> ', req.headers);
 
    passport.authenticate('jwt' ,{ session: false })(req , res , next) 

}

exports.isAuthenticated = ( req , res , next ) =>{

    if(req.user){
        console.log('req.user :>> ', req.user);
        next()
    }else{
        res.status(401).json({
            errorMsg : `Authentication Fails`
        })
    }

}

exports.isAdmin = ( req , res , next ) =>{

    let {user} = req

    if( user ){
        let { role }=  user
        role === 'admin' ? next() : res.status(501).json({msg : `you r not authorized`})
    }
}