
const path = require('path');
let user_services = require('../user_services')

exports.registerUser = async ( req , res ) =>{

    let newUserCredentials = {
        username : req.body.username ? req.body.username : null ,
        password : req.body.password ? req.body.password : null 
    }

    let newUser = await user_services.registerUser(newUserCredentials)
    if(newUser){
        res.status(200).json({
            operation : "success" , 
            user      : newUser
        })
    }else{
        res.status(400).json({
            operation : "fails"
        })
    }

}

exports.loginUser = async ( req , res ) =>{
    try{
        
    let credentials = {
        username : req.body.username ? req.body.username : null ,
        password : req.body.password ? req.body.password : null
    }

    let jsonwebtoken = await user_services.getUserJwt( credentials )

    !jsonwebtoken && res.status(400).json({ "login In" : "Failed "})

    res.setHeader('Authorization' , `bearer ${jsonwebtoken.jwt}`)
    res.status(200).json( {
        successMsg : "login success",
        jsonwebtoken : `bearer ${jsonwebtoken.jwt}`
    })
    // check the integrity of the credentials and issue jwt

    }catch(e){
        res.status(401).send(e)

    }
}

exports.getUserProfile = ( req , res )=>{
    const pathToFile = path.join(__dirname , '..','/profile.html')
    res.sendFile(pathToFile)
}