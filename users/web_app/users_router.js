const express = require('express')
const router = express.Router()
const auth_controller = require('../../Auth/auth_controller')
const user_controller = require('./user_controller')

router.route('/register')
.post( user_controller.registerUser)

router.route('/login')
.post( user_controller.loginUser)

router.route('/profile')
.get(auth_controller.authenticate , 
     auth_controller.isAuthenticated , 
     user_controller.getUserProfile)


module.exports = router