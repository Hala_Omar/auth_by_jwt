
const user_dao = require('./user_dao')
const access_token_services = require('../access_tokens/access_token_services')
const utils    = require('../utils/authentication')

exports.getUserJwt = async ( userCredentials ) =>{

    let user , jwt

    if ( !userCredentials || !userCredentials.password){
        throw new Error({msg : "fill the fieds" , status : 401 })
    }

   user = await utils.checkCredentials( userCredentials ) 
   console.log('user :>> ', user);
   user ? jwt = utils.issueJwt(user) : jwt = null
   // Store the jwt in the database 
   let result = await access_token_services.insertToken(jwt.jwt)
   result && console.log('jwt :>> ', jwt);
   return Promise.resolve(jwt)
}

exports.getUserProfile = ( req , res ) =>{
   
}

exports.registerUser   = ( userCredentials ) =>{

    let { password , username } = userCredentials

    let hash_salt   = utils.getHashAndSalt( password )

    let user = { username , 
                 password : hash_salt.hashedPassword , 
                 salt     : hash_salt.salt , 
                 role     : 'user'}

    return user_dao.addUser(user)

}